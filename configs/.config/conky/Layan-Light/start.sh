#!/bin/bash

killall conky
sleep 2s

conky -c $HOME/.config/conky/Layan-Light/Layan-Light.conf &> /dev/null &
