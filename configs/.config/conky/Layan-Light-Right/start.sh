#!/bin/bash

killall conky
sleep 2s

conky -c $HOME/.config/conky/Layan-Light-Right/Layan-Light-Right.conf &> /dev/null &
