![image info](resources/tema-layan-scuro.png)

Per scaricare velocemente gli scripts utilizzati:

#### Con curl

```shell
bash -c "$(curl -fsSL https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-layan/-/raw/main/get-scripts.sh)"
```

#### Con wget

```shell
bash -c "$(wget https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-layan/-/raw/main/get-scripts.sh -O -)"
```
