# ---------------------------------------------------------------------
# Flat Icon Splash Screens for Gimp 2.10
# Author: migf1 (2020)
#
# The splash screens should also work with older versions of
# Gimp but you may have to copy them to a different folder.
#
# ---------------------------------------------------------------------
# LICENSE:
#
# These splash screens are Licensed under GNU GPL 2 or later,
# with an additional Attribution term!
#
# Meaning that additionally to all GNU GPL terms, any derivative
# work must be explicitly identified as such, followed by an easily
# accessible link to my DeviantArt account:
# https://www.deviantart.com/migf1
# ---------------------------------------------------------------------
# This product is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# any later version.
#
# This product is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this product. If not, see <http://www.gnu.org/licenses/>.
#

==========================================
DISCLAIMER
==========================================

Before anything else, please make sure you have understood the
GNU Public License version 2 (or later) terms for these themes.

Including the additional Attribution term! Meaning that additionally
to all GNU GPL terms, any derivative work must be explicitly identified
as such, followed by an easily accessible link to my DeviantArt account:
    https://www.deviantart.com/migf1

You should have received a copy of the GNU General Public License
version 2 (or later) along with this product. If not, see:
    http://www.gnu.org/licenses/


==========================================
INFO
==========================================

These splash screens have been made to visually match my
"Clearlooks Gimp 2.10 Color Hinted Themes"

	https://bit.ly/2ASlprw

If you don't mind them saying Gimp 2.10, they should work with
any version of Gimp capable of handling custom splash screens.

Depending on the Gimp version, they should probably be placed
to a different folder in your hard disk. Here's what the official
docs say for Gimp 2.10:

	https://docs.gimp.org/2.10/en/customize-splashscreen.html

There are BLUE, ORANGE, PINK, RED and TEAL images in the pack,
with unframed (1) and framed (2) variants (framed ones are also
inversed aesthetically). This makes a total of 10 splash images.

All images are Full HD (1920x1080). Feel free to downsize them
if they are too big for your monitor.


==========================================
SETUP
==========================================

The splash images come packed in a zip-file. Unpack it and copy
the included "splashes\" folder, into Gimp's Configuration folder.

Feel free to delete any images you do not want (the rest will be
picked randomly every time you start Gimp).

Finding Gimp's Configuration Folder:
------------------------------------------------------

Gimp's configuration folder may be hidden, so first make sure
your file manager is set to show hidden items:
    on Windows: https://bit.ly/38cTkr1
    on Linux: https://bit.ly/3i5TYv5
    on macOS: https://bit.ly/2YFRQTb

To locate Gimp's configuration folder, run Gimp and go to:
	Edit -> Preferences -> Folders -> Brushes
	(expand the Folders branch if needed)

If only 1 folder gets listed on the right, select it. If there is more,
select one that resides inside your user-profile folder.

Then click on the icon at the far right, right above the listed folders.
Its mouse-hover label should say: "Show file location in the file manager".

This should open a new window, targeting Gimp's configuration
folder. Notice that you do NOT want the "brushes\" sub-folder,
you want its PARENT folder.

Normally, that folder should be something similar to the following:

On Windows:
  C:\Users\[your-username]\AppData\Roaming\GIMP\2.10

On macOS:
  /Users/[your-username]/Library/Application Support/GIMP/2.10

On Linux (if Gimp installed from repository):
  ~/.config/GIMP/2.10

On Linux (if Gimp installed from Flatpak):
  ~/.var/app/org.gimp.GIMP/config/GIMP/2.10

On Linux (if Gimp installed from Snap):
  ~/snap/gimp/current/.config/GIMP/2.10


==========================================
RELEASE HISTORY
==========================================

v0.1:
� Initial Release (tested on Windows)
